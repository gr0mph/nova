
Everything can be improved

# Howto Contributing

1. Make a team (Product Owner, SCRUM Master, one to six developper)
2. Get list of backlog, choose your backlog
3. Start from developper branch
4. Create a feature with one user story

# Backlog product

## Feature

| Has started | User story |
|:------------|:-----------|
|             | As a new customer, I would like to be able to access a new website in order to collect information. |
|             | As a customer, I want to be able to reference my own experiences in order to share them with other customers. |
|             | As a product owner, I want to be able to rank the quality of the information so that users can find the best information. |
|             | As a product, I want to give people free access to collective intelligence in order to increase collective awareness. |

## Theme

| Has started | User story |
|:------------|:-----------|
|             | As user story 0, I want to access the "information a" page of the site when the user searches on his website for "information site a" |
|             | as user story 0, I would like to see a list of classified information related to "information a" in order to access the best "information a". |
|             | As user story 0, I need a minimal user interface to validate user story 0. |
